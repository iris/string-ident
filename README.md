**This project is deprecated; its functionality has been included in Iris proper
starting with version `dev.2021-03-23.2.dce194f3` (which will eventually be
included in Iris 3.5).**

# Ltac2 implementation of `string_to_ident`

This "Iris plugin" implements the `string_to_ident` hook of the Iris proof mode
using Ltac2, providing support for Gallina names in Iris intro patterns on Coq
8.11 and later.

## Installation

You can install a Coq 8.11 version of this "plugin" via opam. First add the
Iris opam repository:

```sh
opam repo add iris-dev https://gitlab.mpi-sws.org/iris/opam.git
```

Then install with `opam install coq-iris-string-ident`.

## Usage

Before using the feature in the proof mode, simply import this file _after_
the proof mode:

```coq
From iris.proofmode Require Import tactics.
From iris_string_ident Require Import ltac2_string_ident.

Lemma sep_demo {PROP: sbi} (P1: PROP)  (P2 P3: Prop) (Himpl: P2 -> P3) :
  P1 ∗ ⌜P2⌝ -∗ P1 ∗ ⌜P3⌝.
Proof.
  iIntros "[HP %HP2]".
  iFrame.
  iPureIntro.
  exact (Himpl HP2).
Qed.
```

## Acknowledgments

This implementation strategy was suggested by Robbert Krebbers.
